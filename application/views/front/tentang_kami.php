<!DOCTYPE html>
<!--[if IE 9]>
<html class="ie ie9" lang="en-US">
<![endif]-->
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta name="author" content="Car Dealer Template">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <base href="<?php echo base_url() ?>">

    <title>Surya Sentosa - Dealer Daihatsu</title>

	<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="front/assets/css/bootstrap.css">
	<link rel="stylesheet" href="front/assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="front/assets/css/main.css">
	<!-- Slider Pro Css -->
	<link rel="stylesheet" href="front/assets/css/sliderPro.css">
	<!-- Owl Carousel Css -->
	<link rel="stylesheet" href="front/assets/css/owl-carousel.css">
	<!-- Flat Icons Css -->
	<link rel="stylesheet" href="front/assets/css/flaticon.css">
	<!-- Animated Css -->
	<link rel="stylesheet" href="front/assets/css/animated.css">


	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->

</head>
<body>

	
	
	<?php include 'preloader.php'; ?>
	
	<div id="search">
	    <button type="button" class="close">×</button>
	    <form>
	        <input type="search" value="" placeholder="type keyword(s) here" />
	        <button type="submit" class="primary-button"><a href="#">Search <i class="fa fa-search"></i></a></button>
	    </form>
	</div>
	
	<?php include 'header.php'; ?>


	<div class="page-heading wow fadeIn" data-wow-duration="0.5s">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="heading-content-bg wow fadeIn" data-wow-delay="0.75s" data-wow-duration="1s">
						<div class="row">
							<div class="heading-content col-md-12">
								<p><a href="index.php">Beranda</a> / <em> Tentang Kami</em></p>
								<h2>Tentang <em>Kami</em></h2>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	

	<section>
		<div class="more-about-us">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<img src="assets/images/about_us.jpg" alt="">
					</div>
					
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="right-content">
							<span>Profil PT. Surya Sentosa Primatama</span>
							<h4>VISI</h4>
							<p>Menjadi salah satu dealer Terbaik se- sumatera dengan proses
							layanan bisnis kelas nasional yang inovatif, sehat, efesien
							dengan optimalisasi potensi sumber daya manusia dan kepuasaan.
							</p>

							<h4>MISI</h4>
							<ul>
								<li> Memberikan Layanan Terbaik stakeholders</li>
								<li> Mengembangkan Potensi Karyawan untuk menghasilkan kinerja dan layanan tingkat nasional </li>
							</ul>
							<br><br>

							<h4>BUDAYA "IC3L4AS"</h4>
							<ul>
								<li> Integrity (Integritas)</li>
								<li> Commitment – (Komitmen </li>
								<li> Care & Share (Peduli) </li>
								<li> Continuos Learning – (Terus Belajar) </li>
								<li> Loyality – (Loyalitas) </li>
							</ul>
							<br>

							<h4>4 AS Workin</h4>
							<ul>
								<li> Kerja Keras</li>
								<li> Kerja Cerdas</li>
								<li> Kerja Tuntas</li>
								<li> Kerja Ikhlas</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="row">

					<?php foreach ($cabang->result() as $rw): ?>
						
					
					<div class="col-md-6">

						<div class="right-content">
							<img src="image/user/<?php echo $rw->foto ?>" alt="<?php echo $rw->foto ?>" style="width:100%">
							<div class="sep-section-heading">
								<h2><em><?php echo $rw->nama_cabang ?></em></h2>

							</div>
							<table class="table">
								<tr>
									<td>Alamat</td>
									<td><?php echo $rw->alamat ?></td>
								</tr>
								<tr>
									<td>Jam Operational Dealer</td>
									<td><?php echo $rw->jam_dealer ?></td>
								</tr>
								<?php if ($rw->jam_service != ''): ?>
									<tr>
										<td>Jam Operational Service </td>
										<td><?php echo $rw->jam_service ?></td>
									</tr>
								<?php endif ?>
								<tr>
									<td>No Telepon </td>
									<td><?php echo $rw->no_telp_dealer ?></td>
								</tr>
								<?php if ($rw->no_telp_service != ''): ?>
									<tr>
										<td>No Booking Service </td>
										<td><?php echo $rw->no_telp_service ?></td>
									</tr>
								<?php endif ?>
								<tr>
									<td>Pelayanan Tersedia</td>
									<td>

										<?php 
										$ply = explode(',', $rw->pelayanan);
										foreach ($ply as $vl) {
											echo '<span class="btn btn-success btn-sm" style="color:white; margin-bottom: 5px;">'.$vl.'</span> ';
										}
										 ?>
									</td>
								</tr>
							</table>
							<!-- <div class="info-list" style="margin-left: 10px;">
								<span> Melayani Penjualan, Service dan Sparepart</span>
							</div> -->
						</div>
					</div>
					
					<?php endforeach ?>

				</div>
				<!-- <div class="row">
					<div class="col-md-6">
						<div class="right-content">
							<img src="https://tinypng.com/images/social/website.jpg" alt="Avatar" style="width:100%">
							<div class="sep-section-heading">
								<h2>Cabang <em>Jambi</em></h2>

							</div>
							<table class="table">
								<tr>
									<td>Alamat</td>
									<td>Jl. Kol. Abunjani No 09 Kel. Selamat Kec. Telanaipura Kota Jambi</td>
								</tr>
								<tr>
									<td>Jam Operational Dealer</td>
									<td>Senin – Sabtu 08.00- 17.00  Minggu 09.00-15.00</td>
								</tr>
								<tr>
									<td>Jam Operational Service </td>
									<td>Senin – Sabtu 08.00- 17.00  Minggu 09.00-15.00 </td>
								</tr>
								<tr>
									<td>No Telepon </td>
									<td>0741 – 670055-670056 Fax 0741-61573</td>
								</tr>
								<tr>
									<td>No Booking Service </td>
									<td>0823-7272-1163</td>
								</tr>
							</table>
							<div class="info-list" style="margin-left: 10px;">
								<span> Melayani Penjualan, Service dan Sparepart</span>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="right-content">
							<div class="sep-section-heading">
								<h2>Cabang <em>Bungo</em></h2>
							</div>
							<table class="table">
								<tr>
									<td>Alamat</td>
									<td>JL. LINTAS SUMATERA KM 0 SAMPING JASA RAHARJA,KEL. PASIR PUTIH, KEC. RIMBO TENGAH, KAB. BUNGO-JAMBI</td>
								</tr>
								<tr>
									<td>Jam Operational Dealer</td>
									<td>Senin – Sabtu : 08.00 – 17.00</td>
								</tr>
								<tr>
									<td>No Telepon </td>
									<td>0812-2100-5840</td>
								</tr>
							</table>
							<div class="info-list" style="margin-left: 10px;">
								<span> Melayani Penjualan dan Service</span>
							</div>
						</div>
					</div>

					<div class="col-md-6">
						<div class="right-content">
							<div class="sep-section-heading">
								<h2>Cabang <em>Sarolangun </em></h2>
							</div>
							<table class="table">
								<tr>
									<td>Alamat</td>
									<td>Jl. Lintas Sumatra KM. 0 Simp. 4 Lampu Merah, Pasae Atas, Sarolangun</td>
								</tr>
								<tr>
									<td>Jam Operational Dealer</td>
									<td>Senin – Sabtu : 08.00 – 17.00</td>
								</tr>
								<tr>
									<td>No Telepon </td>
									<td>0745-92007 / +62 852-6167-3316</td>
								</tr>
							</table>
							<div class="info-list" style="margin-left: 10px;">
								<span> Melayani Penjualan</span>
							</div>
						</div>
					</div>

					<div class="col-md-6">
						<div class="right-content">
							<div class="sep-section-heading">
								<h2>Cabang <em>Pal VI </em></h2>
							</div>
							<table class="table">
								<tr>
									<td>Alamat</td>
									<td>Jl. Pangeran Hidayat KM. 6 Kota Baru Kota jambi</td>
								</tr>
								<tr>
									<td>Jam Operational Dealer</td>
									<td>Senin – Sabtu : 08.00 – 17.00</td>
								</tr>
								<tr>
									<td>No Telepon </td>
									<td>0853-78701-0880</td>
								</tr>
							</table>
							<div class="info-list" style="margin-left: 10px;">
								<span> Melayani Penjualan</span>
							</div>
						</div>
					</div>


				</div> -->
			</div>
		</div>
	</section>

	


	<?php include 'footer.php'; ?>

	<script src="front/assets/js/jquery-1.11.0.min.js"></script>

	<!-- Slider Pro Js -->
	<script src="front/assets/js/sliderpro.min.js"></script>

	<!-- Slick Slider Js -->
	<script src="front/assets/js/slick.js"></script>

	<!-- Owl Carousel Js -->
    <script src="front/assets/js/owl.carousel.min.js"></script>

	<!-- Boostrap Js -->
    <script src="front/assets/js/bootstrap.min.js"></script>

    <!-- Boostrap Js -->
    <script src="front/assets/js/wow.animation.js"></script>

	<!-- Custom Js -->
    <script src="front/assets/js/custom.js"></script>

</body>

</html>