<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="footer-item">
						<div class="about-us">
							<h2>Media Sosial</h2>
							<p>Ikuti Sosial Media kami.</p>
							<ul>
								<li><a href="#"><i class="fa fa-instagram"></i> @sahabatdaihatsujambi</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="footer-item">
						<div class="what-offer">
							<h2>Alamat kami</h2>
							<p>
								

								Jl. Kol. Abunjani No.09 Sipin-Jambi <br>

								Telp : (0741) 670055 - 670056 <br>

								Email : - 
							</p>
						</div>
					</div>
				</div>
				
				
				<div class="col-md-4">
					<div class="footer-item">
						<div class="quick-search">
							<h2>Pencarian</h2>
							<input type="text" class="footer-search" name="s" placeholder="Search..." value="">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="sub-footer">
						<p>Copyright <?php echo date('Y') ?>. All rights reserved by: <a href="#">Surya Sentosa</a></p>
					</div>
				</div>
			</div>
		</div>
	</footer>